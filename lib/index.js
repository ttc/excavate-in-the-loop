
import * as _ from 'lodash';

import {
  transformFileName,
  createNewFileStructure,
  listFiles,
  filterStringsEndingWith,
  filterStringsIfHasSubstring,
  pullStringsIfHasSubstring,
  readFileSync,
  extractString,
  createMetadata,
  createUUID
} from './utils';

// Source directory for in-the-loop newsletter mails. This should be a copy of the mailman folder.
const ITLsource = 'in-the-loop';

// Destination directory
const destDir = '_site';

// Delimiters for the mail body text
const bodyDelimiterStart = '<PRE>';
const bodyDelimiterEnd = '</PRE>';

// Delimiters for the mail title
const titleDelimiterStart = '<TITLE>';
const titleDelimiterEnd = '</TITLE>';

// Delimiters for the mail date
const dateDelimiterStart = '<I>';
const dateDelimiterEnd = '</I>';

const author = 'Tactical technology Collective';
const uuid = createUUID();

// Filter the files from mailman.
// What we want are files in the format /in-the-loop/2012-August/000017.html
let mailFiles = filterStringsEndingWith(listFiles(ITLsource), 'html');
mailFiles = filterStringsIfHasSubstring(mailFiles, '000');
// Ignoring attachments
mailFiles = pullStringsIfHasSubstring(mailFiles, 'attachment');

_.forEach(mailFiles, file => {
  // Get the mails' content
  const fileContent = readFileSync(file);
  const body = extractString(fileContent, bodyDelimiterStart, bodyDelimiterEnd);

  // Get metadata
  const title = extractString(fileContent, titleDelimiterStart, titleDelimiterEnd);
  const date = extractString(fileContent, dateDelimiterStart, dateDelimiterEnd);
  const metadata = {
    title, date, uuid, author, collection: 'in-the-loop'
  };

  // Refactor files and directories and write to file system
  const newFileName = transformFileName(file);
  createNewFileStructure(destDir, newFileName, body, createMetadata(metadata));
});
