import fs from 'fs-extra';
import mkdirp from 'mkdirp';
import * as _ from 'lodash';

/**
 * List files in a folder
 * @param {String} Dir name
 * @returns {Array} Array with full paths to files
 */
export const listFiles = (dir) => {
  let results = [];
  const list = fs.readdirSync(dir);
  list.forEach((file) => {
    const f = `${dir}/${file}`;
    const stat = fs.statSync(f);
    if (stat && stat.isDirectory()) results = results.concat(listFiles(f));
    else results.push(f);
  });
  return results;
};

let inTheLoopItems = [];
/* Transform the file path and name to the convention used on the website
 * @param {String} The path to the file to be transformed
 * @returns {String} Return modified path
 */
export const transformFileName = (file) => {
  // get the middle part of the path f.e. 2013-March
  const oldPath = _.split(file, '/', 2)[1];
  const tmp = _.split(_.lowerCase(_.split(file, '/', 2)[1]), ' ', 2);
  // ... and transform it to the format paths use on the TTC website f.e. loop-march-2013.
  const newPath = `loop-${tmp[1]}-${tmp[0]}`;
  // Check if this path exists already, if not add it to the registry
  if (!_.find(inTheLoopItems, item => item === newPath)) {
    inTheLoopItems = _.concat(inTheLoopItems, newPath);
    return _.replace(file, oldPath, newPath);
  }
  // Create random path if it exists
  return _.replace(file, oldPath, `${newPath}-${_.random(1000, 10000)}`);
};

/* Create the files in the destination folder with
 * the same dir structure than the paths of the TTC website
 * and change file name and extension to index.md
 * @param {String} Destination directory for all files
 * @param {String} File name
 * @param {String} The mail body content to write
 */
export const createNewFileStructure = (destDir, file, mailBody, metadata) => {
  createDir(destDir, file);
  createFile(`${destDir}/${file}`, metadata + mailBody);
  const md = _.replace(`${destDir}/${file}`, /\d+.html/, 'index.md');
  fs.renameSync(`${destDir}/${file}`, md);
  console.log(`Created file ${md}.`);
};

/*  Create frontmatter from metadata object
 *  @param {Object}: the metadata object
 *  @returns {String}: YAML frontmatter
 */
export const createMetadata = (metadata) => {
  const str = _.replace(
    _.replace(JSON.stringify(_.forEach(metadata, (v, k) => `${k}: "${v}"`)), /","/g, '",\n"'),
    /":"/g, '": "'
  );
  return `--- HEAD\n${str.substring(1, str.length - 1)}\n---\n\n`;
};

/**
 * Create a file (sync)
 * @param {String} File name
 * @param {String} Data to write in file
 */
export const createFile = (fileName, data) => {
  fs.writeFileSync(fileName, data);
};

const getDirName = require('path').dirname;
/**
 * Create a directory (sync)
 * @param {String} destDir: The destination folder in which to create the directory
 * @param {String} Path: the folder including subfolders to create
 */
export const createDir = (destDir, path) => {
  mkdirp.sync(getDirName(`${destDir}/${path}`), (err) => {
    if (err) return err;
  });
};

/**
 * Replace character at index of string
 * @param {String} String to modify
 * @param {Integer} Index at which to replace
 * @param {String} The Substring to be replaced
 * @returns {String} Returns modified string
 */
export const setCharAt = (str, index, chr) => {
  if (index > str.length - 1) return str;
  return str.substr(0, index) + chr + str.substr(index + 1);
};

/**
 * A UUID has the format 5de50344-919a-401a-a7e5-48ff689aa327
 * @param {Integer} Length of the uuid
 * @returns {String} UUID
 */
export const createUUID = (uuidLength = 26) => {
  let s = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  s = Array(uuidLength).join().split(',').map(() => s.charAt(Math.floor(Math.random() * s.length)))
  .join('');
  s = setCharAt(s, 8, '-');
  s = setCharAt(s, 13, '-');
  return setCharAt(s, 19, '-').toLowerCase();
};

/**
 * Filter array to strings which are ending with a substring
 * @param {Array} strings: the array to filter
 * @param {Array} endsWith: the substring at the end of the string to filter for
 * @return {Array} filtered array
 */
export const filterStringsEndingWith = (strings, endsWith) =>
  _.filter(strings, s =>
    _.endsWith(s.toString(), endsWith)
  );

/**
 * Filter array to strings which contain substring
 * @param {Array} strings: the array to filter
 * @param {Array} substring: the substring to filter for
 * @return {Array} filtered array
 */
export const filterStringsIfHasSubstring = (strings, substring) =>
  _.filter(strings, s => _.includes(s, substring));

/**
 * Pull strings which contain substring from array
 * @param {Array} strings: the array to filter
 * @param {Array} substring: the substring to filter for
 * @return {Array} filtered array
 */
export const pullStringsIfHasSubstring = (strings, substring) =>
  _.remove(strings, s => !_.includes(s, substring));

/**
 * read file (sync) in utf8 format
 * @param {String} path to file to read
 * @return {String} files content
 */
export const readFileSync = (file) =>
  fs.readFileSync(file, 'utf8');

/**
 * Extract string between delimiterStart and delimiterEnd
 * @param {String} path to file to read
 * @param {String} delimiterStart
 * @param {String} delimiterEnd
 * @return {String} extracted string
 */
export const extractString = (string, delimiterStart, delimiterEnd) =>
  string.substring(
    string.indexOf(delimiterStart) + delimiterStart.length, string.indexOf(delimiterEnd)
  );


export default {
  createDir,
  listFiles,
  transformFileName,
  createNewFileStructure,
  createFile,
  filterStringsEndingWith,
  filterStringsIfHasSubstring,
  pullStringsIfHasSubstring,
  readFileSync,
  createMetadata,
  createUUID
};
